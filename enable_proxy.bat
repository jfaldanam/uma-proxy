@ECHO OFF

REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings" /v ProxyEnable /t REG_DWORD /d 1 /f

REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings" /v ProxyServer /t REG_SZ /d http://proxy.wifi.uma.es:3128 /f

call git config --global http.proxy http://proxy.wifi.uma.es:3128

start ms-settings:network-proxy
